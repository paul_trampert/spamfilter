1.9.2
-API Version number update.

1.9.1
-/sftest now shows the normalized message that is used against the rule.
-Added unignore button in settings - clears ignore list.
-Hopefully addressed hard crashes.
-Heuristic updates.

1.9
-Added guild invite filtering.
-Fixed color stripping at the beginning of a message.
-/sfadd now checks that both name and filter definition are present.

1.8
-Added color filtering.
-Many heuristic and default filter updates.

1.7.2
-Added libChat to allow for compatibility with pChat addon.
-Can now toggle whether or not to add offenders to the built in ignore list. This toggle does not affect mail - mail offenders will always be added to the ignore list.

1.7.1
-Fixed issue where flood gate and message length filters were getting applied to mail items.
-Adjustments to heuristic filtering.

1.7
-Mail Scanning: Scans your mail using existing filters when you open your mailbox. NOTE! Scans do not auto-delete mail so as to still allow reporting the player with F1
-Truncation: You can now set SpamFilter to truncate or completely ignore messages that trigger rules. Default is "Truncate."
-Heuristical Scanning (Experimental): New experimental feature that should catch most spammers based on message scoring. Might still be a bit over or under-zealous. Use at your own risk.
-Possible fix for crashes due to accented characters in patterns.

1.6.1
-Fixed German translations.
-Strings now have ESO chat formatting stripped before being compared against filters and being checked for length.

1.6
-Added flood gate filter. This will block anyone who sends more than x messages in t seconds. Defaults for x and t are 3 and 3 respectively. This filter can be edited and/or disabled from the settings menu.
-Added message length filter. This will block anyone who sends a message longer than 750 characters. This filter can be edited and/or disabled from the settings menu.
-Added "/sftest" command. This will allow you to to test your custom filters. Syntax is "/sftest <name> <test phrase>"
-Fixed issue where filters would not be saved properly if they began or ended with square brackets.
-New auto-ignored players should now have a note attached to their ignore record stating the rule that triggered the ignore.
-Custom filters can now be edited from the settings menu.

1.5.1
-Renamed license and README to .md files so as not to confuse ESO.

1.5
-German localization added, courtesy of Smaxx (Thank you Smaxx!)
-Copy of GPLv3 added to the package.
-README.txt added.
-Project description changed to match the README.
-Slight modifications to slash commands.

1.4
-Integrated SpamFilter Menu Patch by SLATE (Thank you SLATE!)
-Incoming chat messages are converted to lower-case before being matched. This is to alleviate the need for every letter being an upper or lower character class. This has the side effect of necessitating that all patterns be written in lower case.

1.3
-Fixed persistence bug.
-Removed /sfreset. It doesn't work right with the persistence fix. That's ok though because for whatever reason the default filter is *always* loaded when the plugin is loaded. Still haven't figured this part out yet.

1.2
-Refactored to allow for easier localization.
-Improved default filter.
-Friends will no longer be ignored.

1.1
-Fixed issue with leading/trailing whitespace on filter definitions.

1.0
-Initial release