#SpamFilter
*An Elder Scrolls Online Addon by Paul Trampert (pjt15253)*

**README Updated**
April 10, 2014

**THIS WORK IS COVERED UNDER THE GPLv3**
For full explanation of this license, see license.md included in this package, or goto https://www.gnu.org/licenses/gpl-3.0.txt

**Repository Location**

https://bitbucket.org/paul_trampert/spamfilter/

**Addon Hosted At**

http://www.esoui.com/downloads/fileinfo.php?id=89

http://teso.curseforge.com/teso-addons/spamfilter/

**Special Thanks**

* **Smaxx** - Regular contributor and translator
* **SLATE** - Added graphical settings menu for SpamFilter.
* **The creators of the ESOHead Addon** - I used that addon's code as an example to get started writing Lua.
* **The Creators of wiki.esoui.com** - Awesome documentation on the ESO API.

##Getting Started

###Installation
Extract the SpamFilter directory into your ESO AddOns directory.
If ESO is already running, enter '/reloadui' into chat.

###What this addon does
SpamFilter filters zone, say, and yell channels and in-game mail in ESO's in-game chat for gold spammers, and automatically adds them to your ignore list. Don't worry though - people in your friends list are automatically white-listed, so you don't have to worry about accidentally ignoring a friend.

###Filters
SpamFilter uses filters to detect gold spammers. Regular chatter won't get anyone ignored by accident, unless they indeed write down one of the URLs on the blacklist. Skip the rest of this section unless you're really interested in the technical realization/background.

These are either plain text (like partial URLs) or complex patterns utilizing Lua's regular expression patterns. More information can be found in (Lua User's Patterns Tutorial)[http://lua-users.org/wiki/PatternsTutorial]. They are very similar to Perl's and ECMA's regular expressions, though they have a few notable limitations by comparison:

- You cannot match repeating capture groups (e.g. - (group)* will not work)
- There is no "or" operator
- The escape character is % instead of \

At the time of this writing, SpamFilter comes with 6 built-in filters for the most common spammers encountered so far:

- `%s*[vw]+%.%w-g[o0][l1]d%w*%.c[o0]m%s*`
- `--www%.mmowin%.com--`
- `g[o0][l1]dah%.c[o0]m`
- `g[o0][l1]dce[o0]%.c[o0]m`
- `tesg[o0][l1]dma[l1][l1]`
- `currency[o0]ffer`
- `www%.pvpb[aá]nk%.c[óo0]m`
- `www%.gameim%.com`
- `www%.g4ez%.com`
- `www%.g4ez%.com`
- `[wv]+.gamecb[o0].c[o0]m`
- `vv ?vv ?vv.gam[li]ng4ez.[c<(][o0]m`
- `gold.*%d+g?=.*instant.*delivery.*24/7`
- `f%s?a%s?s%s?t%s?g%s?o%s?l%s?d%s?s.c[o0]m`

If these filters do not suit you, they may be turned off as a whole in the **Settings** menu or with the **/sfdefault** slash command.

You can also add your own custom filters with the **/sfadd** command:

	/sfadd MyFilter [e3]vilg[o0][l1]d

This command will add a filter by the name **MyFilter**, ignoring anyone who says anything containing a match for `[e3]vilg[o0][l1]d`(like `evilgold` or even `3vilg01d`). Note that filter names may **not** contain whitespaces! Filter definitions may do so, though. Also, do not wrap your filter name or definition in quotes (unless you want quotes to be part of the filter name or definition). Lastly, all filters should be written in lowercase (with the exception of inverted character classes, such as %S or %W).

###Full Command List
SpamFilter has a few slash commands to allow you to manipulate and view the current configuration. Note that you don't have to use them. Basic functionality is accessible through the game's **Settings** menu.

* **/sfdebug** - Toggles debug output (default = off).
	
* **/sfdebug <on|off>** - Explicitly sets debug output on or off.
	
* **/sffilters** - Displays all filters in the chat window.
	
* **/sffilters <default|custom>** - Displays only default or custom filters, respectively.
	
* **/sfignorelist** - Displays user's current ignore list.
	
* **/sfadd <name> <definition>** - Adds a custom filter by the name of **<name>** defined by **<definition>**. If a filter of that name already exists, the filter is updated.
	
* **/sfremove <name>** - Removes a filter by the name of **<name>**. Does nothing if a filter by that name does not exist.
	
* **/sfclear** - Clears all custom filters.
	
* **/sfactive** - Toggles whether or not SpamFilter is active (default = on).
	
* **/sfactive <on|off>** - Explicitly turns SpamFilter on or off.
	
* **/sfdefault** - Toggles default filters (default = on).
	
* **/sfdefault <on|off>** - Explicitly turns default filters on or off.

* **/sftest <name> <test phrase>** - Tests to see if **<test phrase>** is a match for the custom rule, **<name>**.

* **/sfmaxlen** - Toggles whether or not the max message length filter is used (default = on).

* **/sfmaxlen <on|off>** - Explicitly turns the max message length filter on or off.

* **/sfmaxlen <number>** - Sets the max message length (in characters).