﻿-------------
-- Globals --
-------------
if (SpamFilter == nil) then SpamFilter = {} end
local LC = LibStub('libChat-1.0')
SpamFilter.savedVarsName = "SpamFilter_SavedVariables"
SpamFilter.internalDefault = function()
	return {
		debug = false,
		active = true,
		enableDefaultFilters = true,
		enableLengthFilter = true,
		maxMessageLength = 750,
		floodGateEnabled = true,
		floodGateTime = 3,
		floodGateMsgs = 3,
		msgAction = SpamFilter.Arguments.truncate,
		scanMails = true,
		useIgnoreList = true,
		filterColors = SpamFilter.Arguments.strip,
		scanGuildInvites = true
	}
end

SpamFilter.defaultFilters =
	{
		["Default - Conversion"] = "%d+k? only %d%$s*e%s*u%s*r",
		["Default - Conversion 2"] = "%d+k? only %d%$s*u%s*s%s*d",
		["Default - Conversion 3"] = "%dk%deur",
		["Default - Currency Offer"] = "currency[o0]ffer",
		["Default - FASTGOLDS"] = "fastg[o0][il]ds",
		["Default - GAMECBO"] = "[wv]+.gamecb[o0].c[o0]m", -- unescaped dots are intentional
		["Default - GAMEIM"] = "www%.gameim%.com",
		["Default - GAMEIM 2"] = "g[%-%s]*a[%-%s]*m[%-%s]*e[%-%s]*i[%-%s]*m[%-%s]*/\\/\\",
		["Default - GAMING4EZ"] = "vv ?vv ?vv.gam[li]ng4ez.[c<(][o0]m",
		["Default - GAMING4EZ 2"] = "www%.g4ez%.com",
		["Default - GGATM"] = "ggatm",
		["Default - GGATM Colors"] = "%[?gg%]?%w?%w?%w?%w?%[?atm%]?%w?%w?%w?%w?%[?%.",
		["Default - GOLDAH"] = "g[o0]%-?[l1]d%-?ah%.[c<(][o0]m",
		["Default - GOLDCEO"] = "g[o0][l1]dce[o0]",
		["Default - Generic Gold"] = "%s*[vw]+%.%w-g[o0][l1]d%w*%.c[o0]m%s*",
		["Default - Hi Gift"] = "^hi,gift$",
		["Default - Instant Delivery"] = "gold.*%d+g?=.*instant.*delivery.*24/7",
		["Default - LFG Fake"] = "^looking for trade & leveling guild,",
		["Default - LFG Fake 2"] = "^female player looking for lovely guild%.",
		["Default - Little Girl"] = "^i'm just a little girl lost in the moment",
		["Default - MMOWIN"] = "m%s*m%s*[o0]%s*w%s*i%s*n",
		["Default - Newsletter"] = "^enimax daily newsletter$",
		["Default - PVPBANK"] = "p%s?v%s?p%s?b%s?a%s?n%s?k%.c.m",
		["Default - PVPBANK 2"] = "pvp.-bank.-c[o0]m",
		["Default - Sorry Sir"] = "^s[o0]+r+y+,sir$",
		["Default - Starspam"] = "wpwvwpb",
		["Default - Starspam 2"] = "wp%dwv%dwp",
		["Default - TESGOLDMALL"] = "tesg[o0][l1]dma[l1][l1]",
		["Default - Update Details"] = "^incoming update%-details for eso$",
		["Default - Welcome"] = "welcome to buy gold",
		["Default - WOWGL"] = "w[o0]wgl.*c[o0]m",
		["Default - WOWGL Stars"] = "%*%*%*%*%*%*%*%*%*%*.-%*%*%*%*%*%*%*%*%*%*.-%*%*%*%*%*%*%*%*%*%*",
		["Default - WOWGL 2"] = "w%-?[o0]%-?w%-?g%-?l",
		["Default - Fake Zone Spam"] = "zone:.-\n+%s*zone:.-\n+\n+",
		["Default - LFDPS 2"] = "l[%s%-%*]*f[%s%-%*]*d[%s%-%*]*p[%s%-%*]*s[%s%-%*]*[c(][%s%-%*]*[%(%)o0]+[%s%-%*]*m",
		["Default - LFDPS 3"] = "l[%s%-%*]*f[%s%-%*]*d[%s%-%*]*p[%s%-%*]*s.-wts.-%d%s*k",
		["Default - WTSITEM"] = "[WV\\/]+[_%s]*I[_%s]*T[_%s]*E[_%s]*[M\\/]+.*p[o0()]+[w\\/]er",
		["Default - 3ZOOM"] = "3&s*z%s*[o0%s]+/\\/\\",
		["Default - WTSMMO"] = "\\/\\/_T_S_/V\\_/V\\",
	}

SpamFilter.filtersCustom = 
{
	data = {}
}

SpamFilter.heuristicMatches =
{
	["<%s*%d+%s*mi?n?s?"] = 1,
	["%A[$S]%s*%d+"] = 1,
	["%d+%s*[$S]%A"] = 1,
	["[vw]-[vw]-[vw+][%.,]"] = 1,
	["cheapest"] = 2,
	["free"] = 1,
	["[!€$]%s*%d+$"] = 5,
	["%d+%s*K%s*/%s*%d+[,%.]%d+"] = 5,
	["U_S_D"] = 5,
	["%-%-%s*%d+$"] = 5,
	["%-%-%-%-%-"] = 3,
	["cheapest goods"] = 3,
	["cheapest price"] = 3,
	["very cheap"] = 3,
	["cheapest from"] = 3,
	["all kinds of in game currency"] = 5,
	["game currency"] = 3,
	["cheapest %d+"] = 3,
	["dear friend"] = 3,
	["good luck"] = 3,
	["[%.,][c<(]-?[0o]-?m"] = 4,
	["%-%s*[c<(]%-?[0o]%-?m"] = 4,
	["g.?[o0].?[il].?d"] = 1,
	["100.*%%.*sa[vf]e"] = 2,
	["100.*%%.*handwork"] = 2,
	["p[o0]wer[1li]e?ve?[1li]"] = 3,
	["24/7"] = 2,
	["7/24"] = 2,
	["\\/"] = 2,
	["()"] = 2,
	["/\\/\\"] = 5,
	["only %d+[S$]"] = 2,
	["pl%s*1%s*%-%s*%d+"] = 2,
	["instant.*deliver"] = 3,
	["fastest.*deliver"] = 3,
	["cheapest.*game"] = 3,
	["via.*game.*mail"] = 3,
	["c0in"] = 3,
	["g[o0][il]d%s*=%s*%d+%.%d+"] = 3,
	["w%-?[o0]%-?w%-?g%-?[il]"] = 5,
	["m%s*m%s*[o0]%s*w%s*i%s*n"] = 5,
	["g4ez.[c<(][o0óòö]m"] = 5,
	["g[a4]m[i1]ng4ez.[c<(][o0]m"] = 5,
	["gameim.[c<(][o0]m"] = 5,
	["g[o0][il]dah.*[c<(][o0]m"] = 5,
	["g[%s%-]+[o0][%s%-]+[il][%s%-]+d[%s%-]+a[%s%-]+h"] = 5,
	["p%s?v%s?p%s?b%s?a%s?n%s?k"] = 5,
	["g%s*a%s*m%s*e%s*i%s*m"] = 5,
	["currency%s*[o0]ffer"] = 5,
	["se[il]+ cheap g[o0][l1]d"] = 5,
	["se[il][il]%s*g[o0][il]d"] = 5,
	["[wv%s]+.%s*3%s*z%s*[o0%s]+%s*m?%s*.%s*c[%s%-]*[o0]?[%s%-]*m"] = 5,
	["[wv]+%.vg[o0][il]ds%."] = 5,
	["%.C%-?[o0]%-?/\\/\\"] = 5,
	["yes%?%?\r?\n"] = 5,
	["%d+k?%s*=%s*=%d+%$"] = 5,
	["hankwork"] = 8,
	["100%s*%%%s*sa[fv]e"] = 5,
	-- checking these here since they use too many variants
	["wowgl"] = 10,
	["g[*%-%s]*4[*%-%s]*e[*%-%s]*z[*%-%s]*.-c%s*[o0{}%s]+%s*m"] = 10,
	["3%s*z%s*[o0]%s*[o0]%s*m%s*%.%s*c%-?[o0]%-?[m/\\]+"] = 10,
}

SpamFilter.filteredChannels = function()
	return {
		CHAT_CHANNEL_ZONE,
		CHAT_CHANNEL_SAY,
		CHAT_CHANNEL_YELL,
		CHAT_CHANNEL_ZONE_LANGUAGE_1,
		CHAT_CHANNEL_ZONE_LANGUAGE_2,
		CHAT_CHANNEL_ZONE_LANGUAGE_3,
	}
end
SpamFilter.savedVars = {}

SpamFilter.floodGateCounters = {}

SpamFilter.noteNew = nil
SpamFilter.ignoreQueue = {}
SpamFilter.ignoreQueueRunning = false
SpamFilter.unignoreQueue = {}
SpamFilter.unignoreQueueRunning = false

-------------
-- Utility --
-------------

local function ProcessIgnoreQueue()
	for name, _ in pairs(SpamFilter.ignoreQueue) do
		AddIgnore(name)
		SpamFilter.ignoreQueue[name] = nil
		zo_callLater(ProcessIgnoreQueue, 2000)
		return
	end
	zo_callLater(function() SpamFilter.ignoreQueueRunning = false end, 2000)
end

local function ProcessUnignoreQueue()
	for name, _ in pairs(SpamFilter.unignoreQueue) do
		if not SpamFilter.ignoreQueueRunning then
			RemoveIgnore(name)
			SpamFilter.unignoreQueue[name] = nil
		end
		zo_callLater(ProcessUnignoreQueue, 2000)
		return
	end
	zo_callLater(function() SpamFilter.unignoreQueueRunning = false end, 2000)
end

function SpamFilter.InitializeSavedVars()
	SpamFilter.savedVars =
	{
		["internal"] = ZO_SavedVars:NewAccountWide(SpamFilter.savedVarsName, 1, "internal", SpamFilter.internalDefault()),
		["filters"] = ZO_SavedVars:NewAccountWide(SpamFilter.savedVarsName, 1, "filters", SpamFilter.filtersCustom),
	}
	SpamFilter.Debug(SpamFilter.savedVars)
end

function SpamFilter.NormalizeString(text, advanced)
	if (type(text) ~= "string") then d('tried normalizing non-string: '..text) return text end

	text = text:gsub('|H.-|h%[?(.-)%]|h?', '%1') -- remove links
	text = text:lower()
	text = text:gsub('||', '@DASH@') -- escape escaped pipes first
	
	text = text:gsub('|c'..string.rep('[%da-f]', 6), '') -- remove color codes
	text = text:gsub('|r', '') -- remove "reset"
	text = text:gsub('|a[lrc]', '') -- remove text alignment
	
	text = text:gsub('@DASH@', '||') -- unescape pipes

	if (advanced == true) then
		text = text:gsub('á', 'a'):gsub('à', 'a'):gsub('â', 'a'):gsub('ä', 'a')
		text = text:gsub('é', 'e'):gsub('è', 'e'):gsub('ê', 'e')
		text = text:gsub('í', 'i'):gsub('ì', 'i'):gsub('î', 'i')
		text = text:gsub('ó', 'o'):gsub('ò', 'o'):gsub('ô', 'o'):gsub('ö', 'o')
		text = text:gsub('ú', 'u'):gsub('ù', 'u'):gsub('û', 'u'):gsub('ü', 'u')
		text = text:gsub('[%*%^_|]+', '')
	end

	return text
end

local function FloodGateCount(name)
	local t = GetTimeStamp()
	SpamFilter.Debug("Counting for "..name.." at "..t)
	if (SpamFilter.floodGateCounters[name] == nil) then
		SpamFilter.Debug("New counter")
		SpamFilter.floodGateCounters[name] = {flowStart = t, msgCount = 1}
		return false
	elseif (SpamFilter.savedVars["internal"].floodGateTime <= GetDiffBetweenTimeStamps(t, SpamFilter.floodGateCounters[name].flowStart)) then
		SpamFilter.Debug("No flood. Diff is "..tostring(GetDiffBetweenTimeStamps(t, SpamFilter.floodGateCounters[name].flowStart)))
		SpamFilter.floodGateCounters[name] = nil
		return false
	else
		SpamFilter.floodGateCounters[name].msgCount = SpamFilter.floodGateCounters[name].msgCount + 1
		SpamFilter.Debug(tostring(SpamFilter.floodGateCounters[name].msgCount).." msgs in flood")
		return SpamFilter.floodGateCounters[name].msgCount >= SpamFilter.savedVars["internal"].floodGateMsgs
	end
end

local function IsIn(value, array)
	SpamFilter.Debug(value)
	SpamFilter.Debug(array)
	if (#array == 0) then
		return false
	end
	for i = 1, #array do
		if (array[i] == value) then
			return true
		end
	end
	return false
end

local function Trim(s)
	return (s:gsub("^%s*(.-)%s*$", "%1"))
end

local function TestRule(rule, testPhrase)
	testPhrase = SpamFilter.NormalizeString(testPhrase)
	return string.find(string.lower(testPhrase), rule) ~= nil
end

function HeuristicMatch(name, text)
	name = SpamFilter.NormalizeString(name, true)
	text = SpamFilter.NormalizeString(text, true)
	local score = 0
	
	-- no vocals in character name - very suspicious
	if (name:find("[aeiou]") == nil) then
		score = score + 5
	end
	
	for pattern, add in pairs(SpamFilter.heuristicMatches) do
		if (text:find(pattern)) then
			score = score + add
		end
	end
	if score > 0 then SpamFilter.Debug('Heuristic score: '..score) end
	-- Threshold:
	if (score < 10) then score = 0 end
	return score
end

local function RuleBroken(name, origText, ismail)
	-- normalize the input string
	local text = SpamFilter.NormalizeString(origText)

	if not ismail then
		if (SpamFilter.savedVars["internal"].enableLengthFilter and string.len(text) > SpamFilter.savedVars["internal"].maxMessageLength) then
			return SpamFilter.EmitStrings.maxMessageLength
		end

		if (SpamFilter.savedVars["internal"].floodGateEnabled and FloodGateCount(name)) then
			return SpamFilter.EmitStrings.floodGate
		end
		
		if (SpamFilter.savedVars["internal"].filterColors == SpamFilter.Arguments.ignore and string.find(origText, "[^|]|[cC]%x%x%x%x%x%x") ~= nil) then
			return SpamFilter.EmitStrings.usedColors
		end
	end
	text = SpamFilter.NormalizeString(origText, true)
	for key, value in pairs(SpamFilter.savedVars["filters"].data) do
		if (TestRule(value, text)) then
			return key
		end
	end
	if (SpamFilter.savedVars["internal"].enableDefaultFilters) then
		for key, value in pairs(SpamFilter.defaultFilters) do
			if (TestRule(value, text)) then
				return key
			end
		end
	end

	if (SpamFilter.savedVars["internal"].heuristicEnabled) then
		local score = HeuristicMatch(name, text)
		if (score > 0) then
			return string.format(SpamFilter.EmitStrings.heuristicRule, score)
		end
	end
	return nil
end

local function Ternary(cond, ifTrue, ifFalse)
	if (cond) then return ifTrue end
	return ifFalse
end

local function EmitMessage(message)
	if (CHAT_SYSTEM) then
		if (message == nil) then
			message = "[nil]"
		elseif (message == "") then
			message = "[Empty String]"
		end
		CHAT_SYSTEM:AddMessage(message)
	end
end

local function EmitTable(t, indent, tableHistory)
	indent = indent or "."
	tableHistory = tableHistory or {}
	for k, v in pairs(t) do
		local vType = type(v)
		EmitMessage(indent.."("..vType.."): "..tostring(k).." = "..tostring(v))
		if (vType == "table") then
			if (tableHistory[v]) then
				EmitMessage(indent.."Avoiding circular reference...")
			else
				tableHistory[v] = true
				EmitTable(v, indent.." ", tableHistory)
			end
		end
	end
end

local function GetCustomFiltersString()
	local ret = ""
	for name, pattern in pairs(SpamFilter.savedVars["filters"].data) do
		ret = ret..string.format("\n%s: %s", name, pattern)
	end
	if ret == "" then
		return "examplerule: www.somebaddomain.com"
	end
	return ret:sub(2)
end

local function ResetCustomFilter()
	GetWindowManager():GetControlByName("SpamFilter_CustomFiltersEdit"):SetText(GetCustomFiltersString())
end

local function ClearCustomFilter()
	SpamFilter.savedVars["filters"].data = {}
	ResetCustomFilter()
end

local function ParseCustomFilter()
	local patterns = GetWindowManager():GetControlByName("SpamFilter_CustomFiltersEdit"):GetText()
	SpamFilter.savedVars["filters"].data = {}
	for name, pattern in patterns:gmatch("([^\n:]+)%s*:%s*([^\n]+)%s*") do
		if name ~= "examplerule" then
			SpamFilter.SetFilter(name, pattern)
		end
	end
	ResetCustomFilter()
end

function SpamFilter.Debug(...)
	if (SpamFilter.savedVars["internal"].debug) then
		SpamFilter.Emit(...)
	end
end

function SpamFilter.Emit(...)
	for i = 1, select("#", ...) do
		local value = select(i, ...)
		if (type(value) == "table") then
			EmitTable(value)
		else
			EmitMessage(tostring(value))
		end
	end
end

local function LoadStrings()
	local lang = GetCVar("language.2") or "en"
	local supported = false
	for k, _ in pairs(SpamFilter.Lang) do
		if k == lang then supported = true end
	end
	if supported == false then
		lang = "en"
	end
	
	SpamFilter.SlashCommands = SpamFilter.Lang[lang].SlashCommands
	SpamFilter.Arguments = SpamFilter.Lang[lang].Arguments
	SpamFilter.EmitStrings = SpamFilter.Lang[lang].EmitStrings
	SpamFilter.OptionsStrings = SpamFilter.Lang[lang].OptionsStrings
end

function SpamFilter.SetFilter(name, definition)
	local def = Trim(definition)
	local key = Trim(name)
	if (string.find(def, "^%[") or string.find(def, "]$")) then
		def = "%s*"..def.."%s*"
	end
	SpamFilter.savedVars["filters"].data[key] = def
end

function SpamFilter.GetEnabled()
	return SpamFilter.savedVars["internal"].active
end

function SpamFilter.SetEnabled(checkbox)
	SpamFilter.savedVars["internal"].active = checkbox
	SpamFilter.Emit(string.format(SpamFilter.EmitStrings.spamFilterStatus, Ternary(SpamFilter.savedVars["internal"].active, SpamFilter.Arguments.active, SpamFilter.Arguments.inactive)))
end

function SpamFilter.GetDebugEnabled()
	return SpamFilter.savedVars["internal"].debug
end

function SpamFilter.SetDebugEnabled(checkbox)
	SpamFilter.savedVars["internal"].debug = checkbox
	SpamFilter.Emit(string.format(SpamFilter.EmitStrings.debugStatus, Ternary(SpamFilter.savedVars["internal"].debug, SpamFilter.Arguments.on, SpamFilter.Arguments.off)))
end

function SpamFilter.ShowIgnoreList()
	local numIgnored = GetNumIgnored()
	for i = 1, numIgnored do
		local name, note = GetIgnoredInfo(i)
		SpamFilter.Emit(string.format(SpamFilter.EmitStrings.ignoredFormat, name, note))
	end
end

function SpamFilter.GetEnableDefaultFilters()
	return SpamFilter.savedVars["internal"].enableDefaultFilters
end

function SpamFilter.SetEnableDefaultFilters(checkbox)
	SpamFilter.savedVars["internal"].enableDefaultFilters = checkbox
	SpamFilter.Emit(string.format(SpamFilter.EmitStrings.defaultFiltersStatus, Ternary(SpamFilter.savedVars["internal"].enableDefaultFilters, SpamFilter.Arguments.on, SpamFilter.Arguments.off)))
end

function SpamFilter.ShowCustomFilters()
	SpamFilter.Emit(SpamFilter.EmitStrings.customFilters)
	for key, value in pairs(SpamFilter.savedVars["filters"].data) do
		SpamFilter.Emit(string.format(SpamFilter.EmitStrings.filterFormat, key, value))
	end
end

function SpamFilter.ShowDefaultFilters()
	SpamFilter.Emit(SpamFilter.EmitStrings.defaultFilters)
	for key, value in pairs(SpamFilter.defaultFilters) do
		SpamFilter.Emit(string.format(SpamFilter.EmitStrings.filterFormat, key, value))
	end
end

function SpamFilter.GetEnableMaxLength()
	return SpamFilter.savedVars["internal"].enableLengthFilter
end

function SpamFilter.SetEnableMaxLength(checkbox)
	SpamFilter.savedVars["internal"].enableLengthFilter = checkbox
end

function SpamFilter.GetMaxMessageLength()
	return SpamFilter.savedVars["internal"].maxMessageLength
end

function SpamFilter.SetMaxMessageLength(value)
	SpamFilter.savedVars["internal"].maxMessageLength = value
end

function SpamFilter.GetFloodGateEnabled()
	return SpamFilter.savedVars["internal"].floodGateEnabled
end

function SpamFilter.SetFloodGateEnabled(checkbox)
	SpamFilter.savedVars["internal"].floodGateEnabled = checkbox
end

function SpamFilter.GetFloodGateTime()
	return SpamFilter.savedVars["internal"].floodGateTime
end

function SpamFilter.SetFloodGateTime(t)
	SpamFilter.savedVars["internal"].floodGateTime = t
end

function SpamFilter.GetFloodGateMsgs()
	return SpamFilter.savedVars["internal"].floodGateMsgs
end

function SpamFilter.SetFloodGateMsgs(msgs)
	SpamFilter.savedVars["internal"].floodGateMsgs = msgs
end

function SpamFilter.GetHeuristicEnabled()
	return SpamFilter.savedVars["internal"].heuristicEnabled
end

function SpamFilter.SetHeuristicEnabled(value)
	SpamFilter.savedVars["internal"].heuristicEnabled = value
end

function SpamFilter.GetMailsEnabled()
	return SpamFilter.savedVars["internal"].scanMails
end

function SpamFilter.SetMailsEnabled(value)
	SpamFilter.savedVars["internal"].scanMails = value
end

function SpamFilter.GetMsgAction()
	return SpamFilter.savedVars["internal"].msgAction or SpamFilter.Arguments.truncate
end

function SpamFilter.SetMsgAction(action)
	SpamFilter.savedVars["internal"].msgAction = action
end

function SpamFilter.GetUseIgnoreList()
	return SpamFilter.savedVars["internal"].useIgnoreList
end

function SpamFilter.SetUseIgnoreList(checkbox)
	SpamFilter.savedVars["internal"].useIgnoreList = checkbox
end

function SpamFilter.GetFilterColors()
	return SpamFilter.savedVars["internal"].filterColors
end

function SpamFilter.SetFilterColors(action)
	SpamFilter.savedVars["internal"].filterColors = action
end

function SpamFilter.GetInvitesEnabled()
	return SpamFilter.savedVars["internal"].scanGuildInvites
end

function SpamFilter.SetInvitesEnabled(checkbox)
	SpamFilter.savedVars["internal"].scanGuildInvites = checkbox
end

function SpamFilter.ClearIgnoreList()
	if SpamFilter.unignoreQueueRunning then return end
	
	for i = 1, GetNumIgnored() do
		local euserid, enote = GetIgnoredInfo(i)
		if euserid and enote then
			if enote:find("SpamFilter") then
				SpamFilter.unignoreQueue[euserid] = true
			end
		end
	end

	SpamFilter.unignoreQueueRunning = true
	zo_callLater(ProcessUnignoreQueue, 1);
end

--------------------
-- Slash Commands --
--------------------
local function AddSlashCommands()
	SLASH_COMMANDS[SpamFilter.SlashCommands.sfdebug] = function(extra)
		if (extra == nil or extra == "") then
			SpamFilter.savedVars["internal"].debug = not SpamFilter.savedVars["internal"].debug
		elseif (extra == SpamFilter.Arguments.on) then
			SpamFilter.savedVars["internal"].debug = true
		elseif (extra == SpamFilter.Arguments.off) then
			SpamFilter.savedVars["internal"].debug = false
		else
			SpamFilter.Emit(string.format(SpamFilter.EmitStrings.invalidArg, tostring(extra)))
			return
		end
		SpamFilter.Emit(string.format(SpamFilter.EmitStrings.debugStatus,
			Ternary(SpamFilter.savedVars["internal"].debug, SpamFilter.Arguments.on, SpamFilter.Arguments.off)))
	end

	SLASH_COMMANDS[SpamFilter.SlashCommands.sffilters] = function(extra)
		local strExtra = tostring(extra)
		SpamFilter.Emit(SpamFilter.EmitStrings.currentFilters)
		if (strExtra == nil or strExtra == "") then
			SpamFilter.ShowDefaultFilters()
			SpamFilter.ShowCustomFilters()
		elseif (strExtra == SpamFilter.Arguments.default) then
			SpamFilter.ShowDefaultFilters()
		elseif (strExtra == SpamFilter.Arguments.custom) then
			SpamFilter.ShowCustomFilters()
		else
			SpamFilter.Emit(string.format(SpamFilter.EmitStrings.invalidArg, strExtra))
		end
	end

	SLASH_COMMANDS[SpamFilter.SlashCommands.sfignorelist] = function()
		local numIgnored = GetNumIgnored()
		for i = 1, numIgnored do
			local name, note = GetIgnoredInfo(i)
			SpamFilter.Emit(string.format(SpamFilter.EmitStrings.ignoredFormat, name, note))
		end
	end

	SLASH_COMMANDS[SpamFilter.SlashCommands.sfadd] = function(extra)
		local strExtra = tostring(extra)
		if (strExtra == nil or strExtra == "") then
			SpamFilter.Emit(SpamFilter.EmitStrings.sfaddFormat)
			return
		end
		local name, def = string.match(strExtra, "(%S+)(.*)")
		if (name == nil or def == nil) then
			SpamFilter.Emit(SpamFilter.EmitStrings.sfaddFormat)
		end
		SpamFilter.SetFilter(name, def)
		SpamFilter.Emit(string.format(SpamFilter.EmitStrings.filterAdded, name))
		ResetCustomFilter()
	end

	SLASH_COMMANDS[SpamFilter.SlashCommands.sfremove] = function(extra)
		local name = tostring(extra)
		if (name == nil or name == "") then
			SpamFilter.Emit(SpamFilter.EmitStrings.sfremoveFormat)
			return
		end
		SpamFilter.savedVars["filters"].data[name] = nil
		SpamFilter.Debug(SpamFilter.savedVars["filters"].data)
		SpamFilter.Emit(string.format(SpamFilter.EmitStrings.filterRemoved, name))
		ResetCustomFilter()
	end

	SLASH_COMMANDS[SpamFilter.SlashCommands.sfclear] = function()
		SpamFilter.savedVars["filters"].data = {}
		SpamFilter.Debug(SpamFilter.savedVars["filters"].data)
		SpamFilter.Emit(SpamFilter.EmitStrings.filtersCleared)
		ResetCustomFilter()
	end

	SLASH_COMMANDS[SpamFilter.SlashCommands.sfactive] = function(extra)
		if (extra == nil or extra == "") then
			SpamFilter.savedVars["internal"].active = not SpamFilter.savedVars["internal"].active
		elseif (extra == SpamFilter.Arguments.on) then
			SpamFilter.savedVars["internal"].active = true
		elseif (extra == SpamFilter.Arguments.off) then
			SpamFilter.savedVars["internal"].active = false
		else
			SpamFilter.Emit(string.format(SpamFilter.EmitStrings.invalidArg, tostring(extra)))
			return
		end
		SpamFilter.Emit(string.format(SpamFilter.EmitStrings.spamFilterStatus, Ternary(SpamFilter.savedVars["internal"].active, SpamFilter.Arguments.active, SpamFilter.Arguments.inactive)))
	end
	
	SLASH_COMMANDS[SpamFilter.SlashCommands.sfdefault] = function(extra)
		if (extra == nil or extra == "") then
			SpamFilter.savedVars["internal"].enableDefaultFilters = not SpamFilter.savedVars["internal"].enableDefaultFilters
		elseif (extra == SpamFilter.Arguments.on or extra == "on") then
			SpamFilter.savedVars["internal"].enableDefaultFilters = true
		elseif (extra == SpamFilter.Arguments.off or extra == "off") then
			SpamFilter.savedVars["internal"].enableDefaultFilters = false
		else
			SpamFilter.Emit(string.format(SpamFilter.EmitStrings.invalidArg, tostring(extra)))
			return
		end
		SpamFilter.Emit(string.format(SpamFilter.EmitStrings.defaultFiltersStatus, Ternary(SpamFilter.savedVars["internal"].enableDefaultFilters, SpamFilter.Arguments.on, SpamFilter.Arguments.off)))
	end
	
	SLASH_COMMANDS[SpamFilter.SlashCommands.sftest] = function(extra)
		if (extra == nil or extra == "") then
			SpamFilter.Emit(SpamFilter.EmitStrings.sftestFormat)
			return
		else
			local name, testPhrase = string.match(extra, "(%S+)(.*)")
			SpamFilter.Debug(name, testPhrase)
			testPhrase = Trim(testPhrase)
			testPhrase = SpamFilter.NormalizeString(testPhrase, true)
			SpamFilter.Emit(string.format(SpamFilter.EmitStrings.normalizedPhrase, testPhrase))
			if (SpamFilter.savedVars["filters"].data[name] == nil) then
				SpamFilter.Emit(string.format(SpamFilter.EmitStrings.noSuchRule, name))
			elseif (TestRule(SpamFilter.savedVars["filters"].data[name], testPhrase)) then
				SpamFilter.Emit(SpamFilter.EmitStrings.ruleIsMatch)
			else
				SpamFilter.Emit(SpamFilter.EmitStrings.ruleNotMatch)
			end
		end
	end
	
	SLASH_COMMANDS[SpamFilter.SlashCommands.sfmaxlen] = function(extra)
		if (extra == nil or extra == "") then
			SpamFilter.savedVars["internal"].enableLengthFilter = not SpamFilter.savedVars["internal"].enableLengthFilter
		elseif (extra == SpamFilter.Arguments.on or extra == "on") then
			SpamFilter.savedVars["internal"].enableLengthFilter = true
		elseif (extra == SpamFilter.Arguments.off or extra == "off") then
			SpamFilter.savedVars["internal"].enableLengthFilter = false
		elseif (tonumber(extra) ~= nil) then
			SpamFilter.savedVars["internal"].maxMessageLength = tonumber(extra)
		else
			SpamFilter.Emit(string.format(SpamFilter.EmitStrings.invalidArg, tostring(extra)))
			return
		end
		SpamFilter.Emit(string.format(SpamFilter.EmitStrings.maxLengthFilterStatus, Ternary(SpamFilter.savedVars["internal"].enableLengthFilter, SpamFilter.Arguments.on, SpamFilter.Arguments.off), SpamFilter.savedVars["internal"].maxMessageLength))
	end
end

-------------------
-- Event Methods --
-------------------

local function OnMessageReceived(messageType, fromName, text)
	local channel = ZO_ChatSystem_GetChannelInfo()[messageType]
	local channelName = GetChannelName(channel.id)
	local channelLink = nil
	local fromEmit = fromName
	if (channel and channel.format) then
		if (channel.channelLinkable) then
			channelLink = ZO_LinkHandler_CreateChannelLink(channelName)
		end
		if (channel.playerLinkable) then
			fromEmit = ZO_LinkHandler_CreatePlayerLink(fromName)
		end
	end
	if (SpamFilter.savedVars["internal"].active and IsIn(messageType, SpamFilter.filteredChannels()) and not IsFriend(fromName)) then
		SpamFilter.Debug("Validating message from "..fromName.." at "..tostring(GetTimeStamp()))
		local ruleBroken = RuleBroken(fromName, text, false)
		if (not IsIgnored(fromName) and ruleBroken ~= nil) then
			-- Queue the player to be ignored ...
			if (SpamFilter.savedVars["internal"].useIgnoreList) then
				SpamFilter.ignoreQueue[fromName] = true
				
			
				if not SpamFilter.ignoreQueueRunning then
					zo_callLater(ProcessIgnoreQueue, 1);
					SpamFilter.ignoreQueueRunning = true
				end
			
				-- ... and queue the note to be set (since this is updated after event processing)
				SpamFilter.noteNew = string.format(SpamFilter.EmitStrings.note, ruleBroken, GetDateStringFromTimestamp(GetTimeStamp()), GetTimeString())
			end
			SpamFilter.Emit(string.format(SpamFilter.EmitStrings.filtered, fromEmit, ruleBroken))
			if (SpamFilter.savedVars["internal"].msgAction == SpamFilter.Arguments.ignore) then
				return
			end
			local normText = SpamFilter.NormalizeString(text)
			if (channel and channel.format) then
				if (channelLink) then
					return Ternary((SpamFilter.savedVars["internal"].msgAction == SpamFilter.Arguments.truncate and normText:len() > 30), normText:sub(1, 27).."...", normText)
				end
				return Ternary((SpamFilter.savedVars["internal"].msgAction == SpamFilter.Arguments.truncate and normText:len() > 30), normText:sub(1, 27).."...", normText)
			end
		end
	end
	SpamFilter.Debug(SpamFilter.savedVars["internal"].filterColors)
	if (SpamFilter.savedVars["internal"].filterColors == SpamFilter.Arguments.strip) then
		SpamFilter.Debug("Stripping colors...")
		return text:gsub("||", "@DASH@"):gsub("|[cC]%x%x%x%x%x%x", ""):gsub("[^|]|[rR]", ""):gsub("@DASH@", "||")
	end
	return text
end

local function OnIgnoreAdded(_, userid)
	-- let's see if there are any notes to be set
	if SpamFilter.noteNew ~= nil then
		-- look for the correct ignore list entry
		for i = GetNumIgnored(), 1, -1 do
			local euserid = GetIgnoredInfo(i)
			if euserid == userid then
				-- cache the note and set it 2 seconds later (to prevent throttling kicking in)
				local note = SpamFilter.noteNew
				zo_callLater(function() SetIgnoreNote(i, note) end, 2000)
				SpamFilter.noteNew = nil
				return
			end
		end
	end
end

local function OnGuildInviteAdded(eventId, guildId, guildName, index, inviterName)
	SpamFilter.Debug(string.format("GuildId: %d", guildId))
	SpamFilter.Debug(string.format("Guild Name: %s", guildName))
	if (SpamFilter.savedVars["internal"].scanGuildInvites) then
		local ruleBroken = RuleBroken(inviterName, guildName, true)
		if (ruleBroken ~= nil) then
			RejectGuildInvite(guildId)
			if (SpamFilter.savedVars["internal"].useIgnoreList) then
				SpamFilter.ignoreQueue[fromName] = true
				
			
				if not SpamFilter.ignoreQueueRunning then
					zo_callLater(ProcessIgnoreQueue, 1);
					SpamFilter.ignoreQueueRunning = true
				end
			
				-- ... and queue the note to be set (since this is updated after event processing)
				SpamFilter.noteNew = string.format(SpamFilter.EmitStrings.noteInvite, ruleBroken, GetDateStringFromTimestamp(GetTimeStamp()), GetTimeString())
			end
			SpamFilter.Emit(string.format(SpamFilter.EmitStrings.filtered, inviterName, ruleBroken))
		end
	end
end

local function OnMailReceived(num)
	if not SpamFilter.savedVars["internal"].active or not SpamFilter.savedVars["internal"].scanMails then
		return num
	end
	
	local id = GetNextMailId()
	while id do
		local fromUser, fromName, subject, icon, unread, system, service = GetMailItemInfo(id)
		
		if --[[unread and]] not system and not service and not IsFriend(fromUser) and not IsIgnored(fromUser) then
			local text = SpamFilter.NormalizeString(ReadMail(id))
			local ruleBroken = RuleBroken(fromName, subject, true)
			if (ruleBroken == nil) then
				ruleBroken = RuleBroken(fromName, text, true)
			end
			if (ruleBroken ~= nil) then
				-- Queue the player to be ignored ...
				SpamFilter.ignoreQueue[fromName] = true
				
				if not SpamFilter.ignoreQueueRunning then
					zo_callLater(ProcessIgnoreQueue, 1);
					SpamFilter.ignoreQueueRunning = true
				end
				
				-- ... and queue the note to be set (since this is updated after event processing)
				SpamFilter.noteNew = string.format(SpamFilter.EmitStrings.noteMail, ruleBroken, GetDateStringFromTimestamp(GetTimeStamp()), GetTimeString())
				
				SpamFilter.Emit(string.format(SpamFilter.EmitStrings.filteredMail, fromName, ruleBroken))
				
				-- Mark mail as read
				RequestReadMail(id)
				if num > 0 then
					num = num - 1
				end
			end
			
		end
		id = GetNextMailId(id)
	end
	return num
end

local function OnAddOnLoaded(eventCode, addOnName)
	if (addOnName ~= "SpamFilter") then
		return
	end
	LoadStrings()
	SpamFilter.InitializeSavedVars()
	AddSlashCommands()
	LC:registerText(OnMessageReceived)
	SpamFilter.Debug("SpamFilter initialized")

	-- options panel
	LAM = LibStub:GetLibrary("LibAddonMenu-1.0")
	SFpanel = LAM:CreateControlPanel("SpamFilter_OptionsPanel", SpamFilter.OptionsStrings.spamFilterOptions)
		LAM:AddHeader(SFpanel, "SpamFilter_AboutHeader", SpamFilter.OptionsStrings.about)
			LAM:AddDescription(SFpanel, "SpamFilter_AboutDesc", SpamFilter.OptionsStrings.aboutDesc)
		--LAM:AddHeader(SFpanel, "SpamFilter_OptionsHeader", SpamFilter.OptionsStrings.options)
			LAM:AddCheckbox(SFpanel, "SpamFilter_Option_Enable_SpamFilter", SpamFilter.OptionsStrings.enableFiltering, SpamFilter.OptionsStrings.enableFilteringTooltip, SpamFilter.GetEnabled, SpamFilter.SetEnabled)
			LAM:AddCheckbox(SFpanel, "SpamFilter_Option_UseIgnoreList", SpamFilter.OptionsStrings.useIgnoreList, SpamFilter.OptionsStrings.useIgnoreListTooltip, SpamFilter.GetUseIgnoreList, SpamFilter.SetUseIgnoreList)
			LAM:AddButton(SFpanel, "SpamFilter_Option_ClearIgnoreList", SpamFilter.OptionsStrings.clearIgnoreList, SpamFilter.OptionsStrings.clearIgnoreListTooltip, SpamFilter.ClearIgnoreList)
			LAM:AddDropdown(SFpanel, "SpamFilter_Option_MessageActions", SpamFilter.OptionsStrings.msgAction, SpamFilter.OptionsStrings.msgActionTooltip, { SpamFilter.Arguments.doNothing, SpamFilter.Arguments.truncate, SpamFilter.Arguments.ignore }, SpamFilter.GetMsgAction, SpamFilter.SetMsgAction)
		LAM:AddHeader(SFpanel, "SpamFilter_FilterHeader", SpamFilter.OptionsStrings.filter)
			LAM:AddDescription(SFpanel, "SpamFilter_FilterDesc", SpamFilter.OptionsStrings.filterDesc)
			LAM:AddCheckbox(SFpanel, "SpamFilter_Option_Enable_DefaultFilters", SpamFilter.OptionsStrings.enableDefaultFilters, SpamFilter.OptionsStrings.enableDefaultFiltersTooltip, SpamFilter.GetEnableDefaultFilters, SpamFilter.SetEnableDefaultFilters)
			LAM:AddDropdown(SFpanel, "SpamFilter_Option_FilterColors", SpamFilter.OptionsStrings.filterColors, SpamFilter.OptionsStrings.filterColorsTooltip, {SpamFilter.Arguments.doNothing, SpamFilter.Arguments.strip, SpamFilter.Arguments.ignore}, SpamFilter.GetFilterColors, SpamFilter.SetFilterColors)
			LAM:AddCheckbox(SFpanel, "SpamFilter_Option_Enable_MaxLengthFilter", SpamFilter.OptionsStrings.enableMaxLengthFilter, SpamFilter.OptionsStrings.enableMaxLengthFilterTooltip, SpamFilter.GetEnableMaxLength, SpamFilter.SetEnableMaxLength)
			LAM:AddSlider(SFpanel, "SpamFilter_Option_MaxMsgLength", SpamFilter.OptionsStrings.maxMessageLength, SpamFilter.OptionsStrings.maxMessageLengthTooltip, 140, 350, 10, SpamFilter.GetMaxMessageLength, SpamFilter.SetMaxMessageLength)
			LAM:AddCheckbox(SFpanel, "SpamFilter_Option_FloodGateEnabled", SpamFilter.OptionsStrings.enableFloodGate, SpamFilter.OptionsStrings.enableFloodGateTooltip, SpamFilter.GetFloodGateEnabled, SpamFilter.SetFloodGateEnabled)
			LAM:AddSlider(SFpanel, "SpamFilter_Option_FloodGateTime", SpamFilter.OptionsStrings.floodGateTime, SpamFilter.OptionsStrings.floodGateTimeTooltip, 2, 10, 1, SpamFilter.GetFloodGateTime, SpamFilter.SetFloodGateTime)
			LAM:AddSlider(SFpanel, "SpamFilter_Option_FloodGateMsgs", SpamFilter.OptionsStrings.floodGateMsgs, SpamFilter.OptionsStrings.floodGateMsgsTooltip, 2, 10, 1, SpamFilter.GetFloodGateMsgs, SpamFilter.SetFloodGateMsgs)
			LAM:AddCheckbox(SFpanel, "SpamFilter_Option_MailsEnabled", SpamFilter.OptionsStrings.scanMails, SpamFilter.OptionsStrings.scanMailsTooltip, SpamFilter.GetMailsEnabled, SpamFilter.SetMailsEnabled)
			LAM:AddCheckbox(SFpanel, "SpamFilter_Option_InvitesEnabled", SpamFilter.OptionsStrings.scanInvites, SpamFilter.OptionsStrings.scanInvitesTooltip, SpamFilter.GetInvitesEnabled, SpamFilter.SetInvitesEnabled)
			LAM:AddCheckbox(SFpanel, "SpamFilter_Option_HeuristicEnabled", SpamFilter.OptionsStrings.enableHeuristic, SpamFilter.OptionsStrings.enableHeuristicTooltip, SpamFilter.GetHeuristicEnabled, SpamFilter.SetHeuristicEnabled)
			LAM:AddDescription(SFpanel, "SpamFilter_CustomFilterDesc", SpamFilter.OptionsStrings.customFilterDesc)
			LAM:AddDescription(SFpanel, "SpamFilter_CustomFilterLabel", nil, SpamFilter.OptionsStrings.customFilters)
			LAM:AddEditBox(SFpanel, "SpamFilter_CustomFilters", nil, nil, true, function() end, function() end, false, nil)
			editbox = GetWindowManager():GetControlByName("SpamFilter_CustomFilters")
			editbox.bg:SetWidth(510)
			editbox.bg:SetAnchor(LEFT)
			editbox.edit:SetMaxInputChars(4096)
			editbox.edit:SetText(GetCustomFiltersString());
			LAM:AddButton(SFpanel, "SpamFilter_ApplyFilters", SpamFilter.OptionsStrings.apply, nil, ParseCustomFilter)
			LAM:AddButton(SFpanel, "SpamFilter_ResetFilters", SpamFilter.OptionsStrings.reset, nil, ResetCustomFilter)
			LAM:AddButton(SFpanel, "SpamFilter_ClearFilters", SpamFilter.OptionsStrings.clear, nil, ClearCustomFilter)
			
		LAM:AddHeader(SFpanel, "SpamFilter_DebugHeader", SpamFilter.OptionsStrings.debugging)
			LAM:AddDescription(SFpanel, "SpamFilter_DebugDesc", SpamFilter.OptionsStrings.debuggingDesc)
			LAM:AddCheckbox(SFpanel, "SpamFilter_Option_Enable_Debugging", SpamFilter.OptionsStrings.enableDebugging, SpamFilter.OptionsStrings.enableDebuggingTooltip, SpamFilter.GetDebugEnabled, SpamFilter.SetDebugEnabled)

	-- unregister event handler
	EVENT_MANAGER:UnregisterForEvent("SpamFilter", EVENT_ADD_ON_LOADED)
	
	-- register handler to write notes
	EVENT_MANAGER:RegisterForEvent("SpamFilter", EVENT_IGNORE_ADDED, OnIgnoreAdded)
	EVENT_MANAGER:RegisterForEvent("SpamFilter", EVENT_MAIL_NUM_UNREAD_CHANGED, OnMailReceived)
	EVENT_MANAGER:RegisterForEvent("SpamFilter", EVENT_GUILD_INVITE_ADDED, OnGuildInviteAdded)
end

EVENT_MANAGER:RegisterForEvent("SpamFilter", EVENT_ADD_ON_LOADED, OnAddOnLoaded)