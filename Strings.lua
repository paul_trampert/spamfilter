﻿if (SpamFilter == nil) then SpamFilter = {} end

SpamFilter.Lang =
{
	["en"] = {},
	["de"] = {},
}

---------------------
-- English Strings --
---------------------

SpamFilter.Lang["en"].SlashCommands =
{
	sfdebug = "/sfdebug",
	sffilters = "/sffilters",
	sfignorelist = "/sfignorelist",
	sfadd = "/sfadd",
	sfremove = "/sfremove",
	sfclear = "/sfclear",
	sfactive = "/sfactive",
	sfdefault = "/sfdefault",
	sftest = "/sftest",
	sfmaxlen = "/sfmaxlen"
}

SpamFilter.Lang["en"].Arguments =
{
	on       = "on",
	off      = "off",
	active   = "active",
	inactive = "inactive",
	default  = "default",
	custom   = "custom",
	doNothing = "Do Nothing",
	truncate = "Truncate",
	ignore   = "Ignore",
	strip    = "Strip"
}

SpamFilter.Lang["en"].EmitStrings =
{
	invalidArg           = "Invalid argument: %s",
	debugStatus          = "SpamFilter debugging is %s",
	defaultFiltersStatus = "SpamFilter default filters are %s",
	currentFilters       = "|cffffffSpamFilter - Current Filters",
	customFilters        = "|cffffffSpamFilter - Custom Filters",
	defaultFilters       = "|cffffffSpamFilter - Default Filters",
	filterFormat         = "  |cffffffName: |caaaaaa%s    |cffffffFilter: |caaaaaa%s",
	ignoredFormat        = "  |cffffffName: |caaaaaa%s    |cffffffNote: |caaaaaa%s",
	sfaddFormat          = "Command Format: /sfadd <name> <filter definition>",
	filterAdded          = "Filter added: %s",
	sfremoveFormat       = "Command Format: /sfremove <name>",
	sftestFormat         = "Command Format: /sftest <name> <test phrase>",
	filterRemoved        = "Filter removed: %s",
	defaultsRestored     = "Default filters restored.",
	filtersCleared       = "All filters removed",
	spamFilterStatus     = "SpamFilter is %s",
	filtered             = "|cff7777%s is now ignored due to violating rule '%s'.",
	filteredMail         = "|cff7777%s is now ignored due to violating rule '%s' [mail].",
	filteredInvite       = "|cff7777%s is now ignored due to violating rule '%s' [guild invite]",
	note                 = "|caaaaaaBlocked by SpamFilter|r\n\nRule: |caaaaaa%s|r\nTimestamp: |caaaaaa%s %s",
	noteMail             = "|caaaaaaBlocked by SpamFilter (Mail)|r\n\nRule: |caaaaaa%s|r\nTimestamp: |caaaaaa%s %s",
	noteInvite           = "|caaaaaaBlocked by SpamFilter (Guild Invite)|r\n\nRule: |caaaaaa%s|r\nTimestamp: |caaaaaa%s %s",
	ruleIsMatch          = "Rule is a match!",
	ruleNotMatch         = "Rule is not a match!",
	noSuchRule           = "No custom rule named %s",
	maxMessageLength     = "Max Message Length",
	maxLengthFilterStatus= "Message length filter is %s with a max message length of %d",
	floodGate            = "Flood Gate",
	heuristicRule        = "Heuristic (score: %s)",
	usedColors           = "Used Colors",
	normalizedPhrase     = "Normalized Phrase: %s"
}

SpamFilter.Lang["en"].OptionsStrings =
{
	spamFilterOptions           = "SpamFilter",
	about                       = "About",
	aboutDesc                   = "SpamFilter scans zone, say, and yell channels for gold spammers, and automatically adds them to your ignore list. Don't worry though - people in your friends list are automatically white-listed, so you don't have to worry about accidentally ignoring a friend.",
	options                     = "Options",
	enableFiltering             = "Enable Filtering",
	enableFilteringTooltip      = "When enabled, accounts that send messages that meet your filters are automatically ignored.",
	enableDefaultFilters        = "Enable default filter list",
	enableDefaultFiltersTooltip = "When enabled, the default filter list will be used in addition to any custom filters.\n\nThis option should stay enabled.",
	filter                      = "Filters",
	filterDesc                  = "Filters define which chat entries aren't acceptable.",
	customFilterDesc            = "You can define one filter per line. Always start with the filter's unique name, followed by a colon (:) and the actual filter pattern.",
	customFilters               = "Custom Filters",
	apply                       = "Apply",
	reset                       = "Reset",
	clear                       = "Clear",
	debugging                   = "Debugging",
	debuggingDesc               = "You should only touch the following options if you plan on working on the add-on. You're not missing out on anything by ignoring them!",
	showIgnoreList              = "Show Ignore List",
	showIgnoreListTooltip       = "Clicking this will display your current ignore list to the chat window.",
	showCustomFilters           = "Show Custom Filters",
	showCustomFiltersTooltip    = "Clicking this will display your current custom filters to the chat window.",
	showDefaultFilters          = "Show Default Filters",
	showDefaultFiltersTooltip   = "Clicking this will display the default filters to the chat window.",
	enableDebugging             = "Enable Debugging",
	enableDebuggingTooltip      = "Enable SpamFilter debug messages to chat window.\n\nThis option should stay disabled.",
	enableMaxLengthFilter       = "Enable Max Length Filter",
	enableMaxLengthFilterTooltip= "Those who send excessively long messages will be ignored with this option turned on.",
	enableHeuristic             = "Use heuristic |caaaaaa(experimental)",
	enableHeuristicTooltip      = "The heuristic allows SpamFilter to judge messages based on multiple factors. While this has a very high chance to catch yet unknown spammers, it might also catch legitimate players using the wrong terms.",
	maxMessageLength            = "Max Message Length (characters)",
	maxMessageLengthTooltip     = "How long is too long?",
	enableFloodGate             = "Enable Flood Gate",
	enableFloodGateTooltip      = "Ignore anyone who sends too many messages in a set amount of time.",
	floodGateTime               = "Flood Gate Time (seconds)",
	floodGateTimeTooltip        = "How long should messages be counted for?",
	floodGateMsgs               = "Flood Gate Messages",
	floodGateMsgsTooltip        = "How many messages does it take in the above time to trigger an ignore?",
	msgAction                   = "Message Action",
	msgActionTooltip            = "What should be done with messages that trigger rules? Seeing all or part of the message that triggers a rule can be useful in identifying false positives.\n|cffffffDo Nothing:|r You will see the full message that triggered a rule\n|cffffffTruncate:|r Messages that trigger rules will be truncated if longer than 30 characters\n|cffffffIgnore:|r Messages that trigger rules will not be shown",
	scanMails                   = "Scan incoming mails",
	scanMailsTooltip            = "Should mails received by you be scanned and marked as spam?",
	useIgnoreList               = "Use Ignore List",
	useIgnoreListTooltip        = "Sets whether or not to add offenders to the built in ignore list. This setting has no effect on mail scanning.",
	clearIgnoreList             = "Clear Ignore List",
	clearIgnoreListTooltip      = "Removes all ignore list entries created by SpamFilter. Note that this might take anything between a few seconds and more than a minute depending on the amount of entries.",
	filterColors                = "Filter Colors",
	filterColorsTooltip         = "Sets what to do with messages containing custom colors.",
	scanInvites                 = "Scan Guild Invites",
	scanInvitesTooltip          = "Should guild invites be scanned and auto-ignored as spam?"
}

--------------------
-- German Strings --
--------------------

SpamFilter.Lang["de"].Arguments =
{
	on       = "ein",
	off      = "aus",
	active   = "aktiv",
	inactive = "inaktiv",
	default  = "default",
	custom   = "eigen",
	doNothing = "Nichts unternehmen",
	truncate = "Verkürzen",
	ignore   = "Ignorieren",
	strip    = "Bereinigen"
}

SpamFilter.Lang["de"].EmitStrings =
{
	invalidArg           = "Ungültiges Argument: %s",
	debugStatus          = "Der SpamFilter-Debugmodus ist %sgeschaltet.",
	defaultFiltersStatus = "Die SpamFilter-Standardfilter sind %sgeschaltet.",
	currentFilters       = "|cffffffSpamFilter - Aktuelle Filter",
	customFilters        = "|cffffffSpamFilter - Eigene Filter",
	defaultFilters       = "|cffffffSpamFilter - Standardfilter",
	filterFormat         = "  |cffffffName: |caaaaaa%s    |cffffffFilter: |caaaaaa%s",
	ignoredFormat        = "  |cffffffName: |caaaaaa%s    |cffffffNotiz: |caaaaaa%s",
	sfaddFormat          = "Syntax: /sfadd <Filtername> <Filterdefinition>",
	filterAdded          = "Der Filter „%s“ wurde hinzugefügt.",
	sfremoveFormat       = "Syntax: /sfremove <Filtername>",
	sftestFormat         = "Syntax: /sftest <Filtername> <Testzeichenfolge>",
	filterRemoved        = "Der Filter „%s“ wurde entfernt",
	defaultsRestored     = "Die Standardfilter wurden wiederhergestellt.",
	filtersCleared       = "Alle Filter wurden entfernt.",
	spamFilterStatus     = "SpamFilter ist %s.",
	filtered             = "|cff7777%s hat gegen die Regel „%s“ verstoßen und wird nun ignoriert.",
	filteredMail         = "|cff7777%s hat gegen die Regel „%s“ verstoßen und wird nun ignoriert [Post].",
	note                 = "|caaaaaaDurch SpamFilter gesperrt|r\n\nRegel: |caaaaaa%s|r\nZeitpunkt: |caaaaaa%s %s",
	noteMail             = "|caaaaaaDurch SpamFilter gesperrt (Post)|r\n\nRegel: |caaaaaa%s|r\nZeitpunkt: |caaaaaa%s %s",
	ruleIsMatch          = "Die Regel trifft zu!",
	ruleNotMatch         = "Die Regel lieferte keine Übereinstimmung!",
	noSuchRule           = "Es gibt keine eigene Regel mit dem Namen „%s“.",
	maxMessageLength     = "Max. Nachrichtenlänge",
	maxLengthFilterStatus= "Die Überprüfung der Nachrichtenlänge ist %sgeschaltet (%d Zeichen).",
	heuristicRule        = "Heuristik (Punktzahl: %s)"
}

SpamFilter.Lang["de"].OptionsStrings =
{
	spamFilterOptions           = "SpamFilter",
	about                       = "Über",
	aboutDesc                   = "SpamFilter sucht automatisch Goldverkäufer und Spammer in den Chatkanälen „Gebiet“, „Sagen“ und „Rufen“ und fügt diese zu Eurer Liste ignorierter Spieler hinzu. Keine Sorge - Eure Freunde werden automatisch übergangen, Ihr müsst Euch also nicht sorgen, diese versehentlich zu ignorieren.",
	options                     = "Einstellungen",
	enableFiltering             = "SpamFilter aktivieren",
	enableFilteringTooltip      = "Im aktivierten Zustand wird SpamFilter Spieler automatisch ignorieren, wenn diese Euch Nachrichten zusenden, auf die mindetens eine Filterregel zutrifft.",
	enableDefaultFilters        = "Standardfilter",
	enableDefaultFiltersTooltip = "Legt fest, ob die Standardfilter zusätzlich zu den eigenen Filtern verwendet werden sollen.\n\nDiese Option sollte aktiviert bleiben.",
	filter                      = "Filter",
	filterDesc                  = "Filter definieren, welche Chateinträge nicht akzeptabel sind.",
	customFilterDesc            = "Ihr könnt einen Filter pro Zeile definieren. Beginnt jeweils mit einem einzigartigen Namen für den Filter, gefolgt von einem Doppelpunkt (:) und dem jeweiligen Suchmuster.",
	customFilters               = "Eigene Filter",
	apply                       = "Übernehmen",
	reset                       = "Zurücksetzen",
	clear                       = "Löschen",
	debugging                   = "Debugmodus",
	debuggingDesc               = "Ihr solltet die folgenden Einstellungen einfach ignorieren, solange Ihr nicht an der Erweiterung arbeiten möchtet. Ihr verpasst dadurch nichts!",
	showIgnoreList              = "Ignorierte Spieler",
	showIgnoreListTooltip       = "Gibt alle aktuell ignorierten Spieler im Chatfenster aus.",
	showCustomFilters           = "Eigene Filter",
	showCustomFiltersTooltip    = "Gibt alle eigenen Filter im Chatfenster aus.",
	showDefaultFilters          = "Standardfilter",
	showDefaultFiltersTooltip   = "Gibt alle Standardfilter im Chatfenster aus.",
	enableDebugging             = "Debugmodus",
	enableDebuggingTooltip      = "Legt fest, ob SpamFilter interne Statustexte ins Chatfenster schreiben soll.\n\nDiese Option sollte deaktiviert bleiben.",
	enableMaxLengthFilter       = "Längenfilter",
	enableMaxLengthFilterTooltip= "Übermäßig lange Nachrichten führen automatisch zu einer Sperre.",
	enableHeuristic             = "Heuristik verwenden |caaaaaa(experimentell)",
	enableHeuristicTooltip      = "Mit der Heuristik kann SpamFilter erhaltene Nachrichten auf Basis mehrerer Faktoren bewerten. Auch wenn dies eine hohe Chance bietet, noch unbekannte Spammer zu erwischen, so besteht durchaus die Möglichkeit, dass auch legitime Spieler erwischt werden, nur weil sie die falschen Worte wählen.",
	maxMessageLength            = "Max. Nachrichtenlänge",
	maxMessageLengthTooltip     = "Wie lange ist zu lange?",
	enableFloodGate             = "Flooding-Schutz",
	enableFloodGateTooltip      = "Das übermäßige Versenden von Nachrichten führt automatisch zu einer Sperre.",
	floodGateTime               = "Zeitraum (Sekunden)",
	floodGateTimeTooltip        = "Welcher Zeitraum soll für die Zählung betrachtet werden?",
	floodGateMsgs               = "Anzahl der Nachrichten",
	floodGateMsgsTooltip        = "Wie viele Nachrichten sollen im gewählten Zeitraum maximal erlaubt sein?",
	msgAction                   = "Verhaltensweise",
	msgActionTooltip            = "Was soll mit Chatzeilen passieren, die eine Regelverletzung darstellen? Fehlmeldungen können am einfachsten erkannt werden, wenn die komplette Nachricht weiterhin angezeigt wird.\n|cffffffNichts unternehmen:|r Euch wird die komplette Nachricht angezeigt.\n|cffffffVerkürzen:|r Erkannte Nachrichten werden bei maximal 30 Zeichen abgeschnitten.\n|cffffffIgnorieren:|r Erkannte Nachrichten werden bereits unterdrückt.",
	scanMails                   = "Eingehende Post prüfen",
	scanMailsTooltip            = "Soll erhaltene Post überprüft und ggf. als Spam markiert werden?",
	useIgnoreList               = "„Ignorierte Spieler“ verwenden",
	useIgnoreListTooltip        = "Sollten Regelbrecher zu Liste ignorierter Spieler des Spiels hinzugefügt werden oder nicht?",
	clearIgnoreList             = "Liste bereinigen",
	clearIgnoreListTooltip      = "Entfernt alle Einträge von der Liste ignorierter Spieler, die von SpamFitler erstellt worden. Beachtet, dass dies je nach Anzahl der Einträge zwischen wenigen Sekunden und mehreren Minuten dauern kann.",
	filterColors                = "Farbige Nachrichten",
	filterColorsTooltip         = "Was soll mit Nachrichten geschehen, die Farbcodes enthalten?"
}

--------------------------
-- Fill missing strings --
--------------------------

for cat, ecat in pairs(SpamFilter.Lang["en"]) do
	for lc, loc in pairs(SpamFilter.Lang) do
		if lc ~= "en" then
			if loc[cat] == nil then
				loc[cat] = {}
			end
			for k, v in pairs(ecat) do
				if loc[cat][k] == nil then
					loc[cat][k] = v
				end
			end
		end
	end
end
